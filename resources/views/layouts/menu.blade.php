<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('userLocations*') ? 'active' : '' }}">
    <a href="{!! route('userLocations.index') !!}"><i class="fa fa-edit"></i><span>User Locations</span></a>
</li>

<li class="{{ Request::is('wallets*') ? 'active' : '' }}">
    <a href="{!! route('wallets.index') !!}"><i class="fa fa-edit"></i><span>Wallets</span></a>
</li>

<li class="{{ Request::is('locations*') ? 'active' : '' }}">
    <a href="{!! route('locations.index') !!}"><i class="fa fa-edit"></i><span>Locations</span></a>
</li>

<li class="{{ Request::is('typeTransactionCategories*') ? 'active' : '' }}">
    <a href="{!! route('typeTransactionCategories.index') !!}"><i class="fa fa-edit"></i><span>Type Transaction Categories</span></a>
</li>

<li class="{{ Request::is('transactionCategories*') ? 'active' : '' }}">
    <a href="{!! route('transactionCategories.index') !!}"><i class="fa fa-edit"></i><span>Transaction Categories</span></a>
</li>

<li class="{{ Request::is('budgets*') ? 'active' : '' }}">
    <a href="{!! route('budgets.index') !!}"><i class="fa fa-edit"></i><span>Budgets</span></a>
</li>

<li class="{{ Request::is('transactions*') ? 'active' : '' }}">
    <a href="{!! route('transactions.index') !!}"><i class="fa fa-edit"></i><span>Transactions</span></a>
</li>

<li class="{{ Request::is('periods*') ? 'active' : '' }}">
    <a href="{!! route('periods.index') !!}"><i class="fa fa-edit"></i><span>Periods</span></a>
</li>

<li class="{{ Request::is('userConnexions*') ? 'active' : '' }}">
    <a href="{!! route('userConnexions.index') !!}"><i class="fa fa-edit"></i><span>User Connexions</span></a>
</li>

