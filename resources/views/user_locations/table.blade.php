<table class="table table-responsive" id="userLocations-table">
    <thead>
        <tr>
            <th>Client Id</th>
        <th>User Id</th>
        <th>Location Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($userLocations as $userLocation)
        <tr>
            <td>{!! $userLocation->client_id !!}</td>
            <td>{!! $userLocation->user_id !!}</td>
            <td>{!! $userLocation->location_id !!}</td>
            <td>
                {!! Form::open(['route' => ['userLocations.destroy', $userLocation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('userLocations.show', [$userLocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('userLocations.edit', [$userLocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>