<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transaction->id !!}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', 'Client Id:') !!}
    <p>{!! $transaction->client_id !!}</p>
</div>

<!-- Transaction Category Id Field -->
<div class="form-group">
    {!! Form::label('transaction_category_id', 'Transaction Category Id:') !!}
    <p>{!! $transaction->transaction_category_id !!}</p>
</div>

<!-- Wallet Id Field -->
<div class="form-group">
    {!! Form::label('wallet_id', 'Wallet Id:') !!}
    <p>{!! $transaction->wallet_id !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $transaction->amount !!}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{!! $transaction->currency !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $transaction->note !!}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{!! $transaction->location_lat !!}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{!! $transaction->location_lng !!}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{!! $transaction->location_address !!}</p>
</div>

<!-- Location Google Ref Field -->
<div class="form-group">
    {!! Form::label('location_google_ref', 'Location Google Ref:') !!}
    <p>{!! $transaction->location_google_ref !!}</p>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('picture', 'Picture:') !!}
    <p>{!! $transaction->picture !!}</p>
</div>

<!-- Is Repeated Field -->
<div class="form-group">
    {!! Form::label('is_repeated', 'Is Repeated:') !!}
    <p>{!! $transaction->is_repeated !!}</p>
</div>

<!-- Repeated By Field -->
<div class="form-group">
    {!! Form::label('repeated_by', 'Repeated By:') !!}
    <p>{!! $transaction->repeated_by !!}</p>
</div>

<!-- Done At Field -->
<div class="form-group">
    {!! Form::label('done_at', 'Done At:') !!}
    <p>{!! $transaction->done_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $transaction->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $transaction->updated_at !!}</p>
</div>

