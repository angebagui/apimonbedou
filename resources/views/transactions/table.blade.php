<table class="table table-responsive" id="transactions-table">
    <thead>
        <tr>
            <th>Client Id</th>
        <th>Transaction Category Id</th>
        <th>Wallet Id</th>
        <th>Amount</th>
        <th>Currency</th>
        <th>Note</th>
        <th>Location Lat</th>
        <th>Location Lng</th>
        <th>Location Address</th>
        <th>Location Google Ref</th>
        <th>Picture</th>
        <th>Is Repeated</th>
        <th>Repeated By</th>
        <th>Done At</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
            <td>{!! $transaction->client_id !!}</td>
            <td>{!! $transaction->transaction_category_id !!}</td>
            <td>{!! $transaction->wallet_id !!}</td>
            <td>{!! $transaction->amount !!}</td>
            <td>{!! $transaction->currency !!}</td>
            <td>{!! $transaction->note !!}</td>
            <td>{!! $transaction->location_lat !!}</td>
            <td>{!! $transaction->location_lng !!}</td>
            <td>{!! $transaction->location_address !!}</td>
            <td>{!! $transaction->location_google_ref !!}</td>
            <td>{!! $transaction->picture !!}</td>
            <td>{!! $transaction->is_repeated !!}</td>
            <td>{!! $transaction->repeated_by !!}</td>
            <td>{!! $transaction->done_at !!}</td>
            <td>
                {!! Form::open(['route' => ['transactions.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transactions.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transactions.edit', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>