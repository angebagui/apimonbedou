<!-- Client Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', 'Client Id:') !!}
    {!! Form::text('client_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Transaction Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaction_category_id', 'Transaction Category Id:') !!}
    {!! Form::number('transaction_category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Wallet Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wallet_id', 'Wallet Id:') !!}
    {!! Form::number('wallet_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', 'Currency:') !!}
    {!! Form::text('currency', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    {!! Form::number('location_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    {!! Form::number('location_lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_address', 'Location Address:') !!}
    {!! Form::text('location_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Google Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_google_ref', 'Location Google Ref:') !!}
    {!! Form::text('location_google_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->
<div class="form-group col-sm-6">
    {!! Form::label('picture', 'Picture:') !!}
    {!! Form::text('picture', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Repeated Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_repeated', 'Is Repeated:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_repeated', false) !!}
        {!! Form::checkbox('is_repeated', '1', null) !!} 1
    </label>
</div>

<!-- Repeated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repeated_by', 'Repeated By:') !!}
    {!! Form::number('repeated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Done At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('done_at', 'Done At:') !!}
    {!! Form::date('done_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
