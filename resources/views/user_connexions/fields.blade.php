<!-- Device Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_model', 'Device Model:') !!}
    {!! Form::text('device_model', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Os Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_os', 'Device Os:') !!}
    {!! Form::text('device_os', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_id', 'Device Id:') !!}
    {!! Form::text('device_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_info', 'Device Info:') !!}
    {!! Form::text('device_info', null, ['class' => 'form-control']) !!}
</div>

<!-- Network Field -->
<div class="form-group col-sm-6">
    {!! Form::label('network', 'Network:') !!}
    {!! Form::text('network', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Success Field -->
<div class="form-group col-sm-6">
    {!! Form::label('success', 'Success:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('success', false) !!}
        {!! Form::checkbox('success', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('userConnexions.index') !!}" class="btn btn-default">Cancel</a>
</div>
