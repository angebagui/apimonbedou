<table class="table table-responsive" id="userConnexions-table">
    <thead>
        <tr>
            <th>Device Model</th>
        <th>Device Os</th>
        <th>Device Id</th>
        <th>Device Info</th>
        <th>Network</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Success</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($userConnexions as $userConnexion)
        <tr>
            <td>{!! $userConnexion->device_model !!}</td>
            <td>{!! $userConnexion->device_os !!}</td>
            <td>{!! $userConnexion->device_id !!}</td>
            <td>{!! $userConnexion->device_info !!}</td>
            <td>{!! $userConnexion->network !!}</td>
            <td>{!! $userConnexion->latitude !!}</td>
            <td>{!! $userConnexion->longitude !!}</td>
            <td>{!! $userConnexion->success !!}</td>
            <td>
                {!! Form::open(['route' => ['userConnexions.destroy', $userConnexion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('userConnexions.show', [$userConnexion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('userConnexions.edit', [$userConnexion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>