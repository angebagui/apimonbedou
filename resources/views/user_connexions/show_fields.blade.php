<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $userConnexion->id !!}</p>
</div>

<!-- Device Model Field -->
<div class="form-group">
    {!! Form::label('device_model', 'Device Model:') !!}
    <p>{!! $userConnexion->device_model !!}</p>
</div>

<!-- Device Os Field -->
<div class="form-group">
    {!! Form::label('device_os', 'Device Os:') !!}
    <p>{!! $userConnexion->device_os !!}</p>
</div>

<!-- Device Id Field -->
<div class="form-group">
    {!! Form::label('device_id', 'Device Id:') !!}
    <p>{!! $userConnexion->device_id !!}</p>
</div>

<!-- Device Info Field -->
<div class="form-group">
    {!! Form::label('device_info', 'Device Info:') !!}
    <p>{!! $userConnexion->device_info !!}</p>
</div>

<!-- Network Field -->
<div class="form-group">
    {!! Form::label('network', 'Network:') !!}
    <p>{!! $userConnexion->network !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $userConnexion->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $userConnexion->longitude !!}</p>
</div>

<!-- Success Field -->
<div class="form-group">
    {!! Form::label('success', 'Success:') !!}
    <p>{!! $userConnexion->success !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userConnexion->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userConnexion->updated_at !!}</p>
</div>

