<table class="table table-responsive" id="transactionCategories-table">
    <thead>
        <tr>
            <th>Code</th>
        <th>Type Category Id</th>
        <th>Name</th>
        <th>Language</th>
        <th>Logo</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactionCategories as $transactionCategory)
        <tr>
            <td>{!! $transactionCategory->code !!}</td>
            <td>{!! $transactionCategory->type_category_id !!}</td>
            <td>{!! $transactionCategory->name !!}</td>
            <td>{!! $transactionCategory->language !!}</td>
            <td>{!! $transactionCategory->logo !!}</td>
            <td>
                {!! Form::open(['route' => ['transactionCategories.destroy', $transactionCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transactionCategories.show', [$transactionCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transactionCategories.edit', [$transactionCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>