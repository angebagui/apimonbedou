<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div>

<!-- Facebook Id Field -->
<div class="form-group">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    <p>{!! $user->facebook_id !!}</p>
</div>

<!-- Twitter Id Field -->
<div class="form-group">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    <p>{!! $user->twitter_id !!}</p>
</div>

<!-- Google Id Field -->
<div class="form-group">
    {!! Form::label('google_id', 'Google Id:') !!}
    <p>{!! $user->google_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{!! $user->phone_number !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $user->photo !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $user->password !!}</p>
</div>

<!-- Activated Field -->
<div class="form-group">
    {!! Form::label('activated', 'Activated:') !!}
    <p>{!! $user->activated !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $user->updated_at !!}</p>
</div>

