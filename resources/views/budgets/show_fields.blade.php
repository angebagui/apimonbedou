<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $budget->id !!}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', 'Client Id:') !!}
    <p>{!! $budget->client_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $budget->name !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $budget->amount !!}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{!! $budget->currency !!}</p>
</div>

<!-- Transaction Category Id Field -->
<div class="form-group">
    {!! Form::label('transaction_category_id', 'Transaction Category Id:') !!}
    <p>{!! $budget->transaction_category_id !!}</p>
</div>

<!-- Periode Field -->
<div class="form-group">
    {!! Form::label('periode', 'Periode:') !!}
    <p>{!! $budget->periode !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $budget->start_date !!}</p>
</div>

<!-- Notification Enabled Field -->
<div class="form-group">
    {!! Form::label('notification_enabled', 'Notification Enabled:') !!}
    <p>{!! $budget->notification_enabled !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $budget->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $budget->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $budget->updated_at !!}</p>
</div>

