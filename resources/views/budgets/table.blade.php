<table class="table table-responsive" id="budgets-table">
    <thead>
        <tr>
            <th>Client Id</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Currency</th>
        <th>Transaction Category Id</th>
        <th>Periode</th>
        <th>Start Date</th>
        <th>Notification Enabled</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($budgets as $budget)
        <tr>
            <td>{!! $budget->client_id !!}</td>
            <td>{!! $budget->name !!}</td>
            <td>{!! $budget->amount !!}</td>
            <td>{!! $budget->currency !!}</td>
            <td>{!! $budget->transaction_category_id !!}</td>
            <td>{!! $budget->periode !!}</td>
            <td>{!! $budget->start_date !!}</td>
            <td>{!! $budget->notification_enabled !!}</td>
            <td>{!! $budget->user_id !!}</td>
            <td>
                {!! Form::open(['route' => ['budgets.destroy', $budget->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('budgets.show', [$budget->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('budgets.edit', [$budget->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>