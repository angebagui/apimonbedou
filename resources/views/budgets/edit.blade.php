@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Budget
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($budget, ['route' => ['budgets.update', $budget->id], 'method' => 'patch']) !!}

                        @include('budgets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection