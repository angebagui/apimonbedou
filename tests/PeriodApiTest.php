<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PeriodApiTest extends TestCase
{
    use MakePeriodTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePeriod()
    {
        $period = $this->fakePeriodData();
        $this->json('POST', '/api/v1/periods', $period);

        $this->assertApiResponse($period);
    }

    /**
     * @test
     */
    public function testReadPeriod()
    {
        $period = $this->makePeriod();
        $this->json('GET', '/api/v1/periods/'.$period->id);

        $this->assertApiResponse($period->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePeriod()
    {
        $period = $this->makePeriod();
        $editedPeriod = $this->fakePeriodData();

        $this->json('PUT', '/api/v1/periods/'.$period->id, $editedPeriod);

        $this->assertApiResponse($editedPeriod);
    }

    /**
     * @test
     */
    public function testDeletePeriod()
    {
        $period = $this->makePeriod();
        $this->json('DELETE', '/api/v1/periods/'.$period->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/periods/'.$period->id);

        $this->assertResponseStatus(404);
    }
}
