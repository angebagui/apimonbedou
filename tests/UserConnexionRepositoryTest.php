<?php

use App\Models\UserConnexion;
use App\Repositories\UserConnexionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserConnexionRepositoryTest extends TestCase
{
    use MakeUserConnexionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserConnexionRepository
     */
    protected $userConnexionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->userConnexionRepo = App::make(UserConnexionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUserConnexion()
    {
        $userConnexion = $this->fakeUserConnexionData();
        $createdUserConnexion = $this->userConnexionRepo->create($userConnexion);
        $createdUserConnexion = $createdUserConnexion->toArray();
        $this->assertArrayHasKey('id', $createdUserConnexion);
        $this->assertNotNull($createdUserConnexion['id'], 'Created UserConnexion must have id specified');
        $this->assertNotNull(UserConnexion::find($createdUserConnexion['id']), 'UserConnexion with given id must be in DB');
        $this->assertModelData($userConnexion, $createdUserConnexion);
    }

    /**
     * @test read
     */
    public function testReadUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $dbUserConnexion = $this->userConnexionRepo->find($userConnexion->id);
        $dbUserConnexion = $dbUserConnexion->toArray();
        $this->assertModelData($userConnexion->toArray(), $dbUserConnexion);
    }

    /**
     * @test update
     */
    public function testUpdateUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $fakeUserConnexion = $this->fakeUserConnexionData();
        $updatedUserConnexion = $this->userConnexionRepo->update($fakeUserConnexion, $userConnexion->id);
        $this->assertModelData($fakeUserConnexion, $updatedUserConnexion->toArray());
        $dbUserConnexion = $this->userConnexionRepo->find($userConnexion->id);
        $this->assertModelData($fakeUserConnexion, $dbUserConnexion->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $resp = $this->userConnexionRepo->delete($userConnexion->id);
        $this->assertTrue($resp);
        $this->assertNull(UserConnexion::find($userConnexion->id), 'UserConnexion should not exist in DB');
    }
}
