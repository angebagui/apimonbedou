<?php

use App\Models\UserLocation;
use App\Repositories\UserLocationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserLocationRepositoryTest extends TestCase
{
    use MakeUserLocationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserLocationRepository
     */
    protected $userLocationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->userLocationRepo = App::make(UserLocationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUserLocation()
    {
        $userLocation = $this->fakeUserLocationData();
        $createdUserLocation = $this->userLocationRepo->create($userLocation);
        $createdUserLocation = $createdUserLocation->toArray();
        $this->assertArrayHasKey('id', $createdUserLocation);
        $this->assertNotNull($createdUserLocation['id'], 'Created UserLocation must have id specified');
        $this->assertNotNull(UserLocation::find($createdUserLocation['id']), 'UserLocation with given id must be in DB');
        $this->assertModelData($userLocation, $createdUserLocation);
    }

    /**
     * @test read
     */
    public function testReadUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $dbUserLocation = $this->userLocationRepo->find($userLocation->id);
        $dbUserLocation = $dbUserLocation->toArray();
        $this->assertModelData($userLocation->toArray(), $dbUserLocation);
    }

    /**
     * @test update
     */
    public function testUpdateUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $fakeUserLocation = $this->fakeUserLocationData();
        $updatedUserLocation = $this->userLocationRepo->update($fakeUserLocation, $userLocation->id);
        $this->assertModelData($fakeUserLocation, $updatedUserLocation->toArray());
        $dbUserLocation = $this->userLocationRepo->find($userLocation->id);
        $this->assertModelData($fakeUserLocation, $dbUserLocation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $resp = $this->userLocationRepo->delete($userLocation->id);
        $this->assertTrue($resp);
        $this->assertNull(UserLocation::find($userLocation->id), 'UserLocation should not exist in DB');
    }
}
