<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserConnexionApiTest extends TestCase
{
    use MakeUserConnexionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUserConnexion()
    {
        $userConnexion = $this->fakeUserConnexionData();
        $this->json('POST', '/api/v1/userConnexions', $userConnexion);

        $this->assertApiResponse($userConnexion);
    }

    /**
     * @test
     */
    public function testReadUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $this->json('GET', '/api/v1/userConnexions/'.$userConnexion->id);

        $this->assertApiResponse($userConnexion->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $editedUserConnexion = $this->fakeUserConnexionData();

        $this->json('PUT', '/api/v1/userConnexions/'.$userConnexion->id, $editedUserConnexion);

        $this->assertApiResponse($editedUserConnexion);
    }

    /**
     * @test
     */
    public function testDeleteUserConnexion()
    {
        $userConnexion = $this->makeUserConnexion();
        $this->json('DELETE', '/api/v1/userConnexions/'.$userConnexion->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/userConnexions/'.$userConnexion->id);

        $this->assertResponseStatus(404);
    }
}
