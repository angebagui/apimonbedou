<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WalletApiTest extends TestCase
{
    use MakeWalletTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateWallet()
    {
        $wallet = $this->fakeWalletData();
        $this->json('POST', '/api/v1/wallets', $wallet);

        $this->assertApiResponse($wallet);
    }

    /**
     * @test
     */
    public function testReadWallet()
    {
        $wallet = $this->makeWallet();
        $this->json('GET', '/api/v1/wallets/'.$wallet->id);

        $this->assertApiResponse($wallet->toArray());
    }

    /**
     * @test
     */
    public function testUpdateWallet()
    {
        $wallet = $this->makeWallet();
        $editedWallet = $this->fakeWalletData();

        $this->json('PUT', '/api/v1/wallets/'.$wallet->id, $editedWallet);

        $this->assertApiResponse($editedWallet);
    }

    /**
     * @test
     */
    public function testDeleteWallet()
    {
        $wallet = $this->makeWallet();
        $this->json('DELETE', '/api/v1/wallets/'.$wallet->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/wallets/'.$wallet->id);

        $this->assertResponseStatus(404);
    }
}
