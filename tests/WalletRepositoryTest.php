<?php

use App\Models\Wallet;
use App\Repositories\WalletRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WalletRepositoryTest extends TestCase
{
    use MakeWalletTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var WalletRepository
     */
    protected $walletRepo;

    public function setUp()
    {
        parent::setUp();
        $this->walletRepo = App::make(WalletRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateWallet()
    {
        $wallet = $this->fakeWalletData();
        $createdWallet = $this->walletRepo->create($wallet);
        $createdWallet = $createdWallet->toArray();
        $this->assertArrayHasKey('id', $createdWallet);
        $this->assertNotNull($createdWallet['id'], 'Created Wallet must have id specified');
        $this->assertNotNull(Wallet::find($createdWallet['id']), 'Wallet with given id must be in DB');
        $this->assertModelData($wallet, $createdWallet);
    }

    /**
     * @test read
     */
    public function testReadWallet()
    {
        $wallet = $this->makeWallet();
        $dbWallet = $this->walletRepo->find($wallet->id);
        $dbWallet = $dbWallet->toArray();
        $this->assertModelData($wallet->toArray(), $dbWallet);
    }

    /**
     * @test update
     */
    public function testUpdateWallet()
    {
        $wallet = $this->makeWallet();
        $fakeWallet = $this->fakeWalletData();
        $updatedWallet = $this->walletRepo->update($fakeWallet, $wallet->id);
        $this->assertModelData($fakeWallet, $updatedWallet->toArray());
        $dbWallet = $this->walletRepo->find($wallet->id);
        $this->assertModelData($fakeWallet, $dbWallet->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteWallet()
    {
        $wallet = $this->makeWallet();
        $resp = $this->walletRepo->delete($wallet->id);
        $this->assertTrue($resp);
        $this->assertNull(Wallet::find($wallet->id), 'Wallet should not exist in DB');
    }
}
