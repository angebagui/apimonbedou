<?php

use App\Models\TransactionCategory;
use App\Repositories\TransactionCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransactionCategoryRepositoryTest extends TestCase
{
    use MakeTransactionCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TransactionCategoryRepository
     */
    protected $transactionCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->transactionCategoryRepo = App::make(TransactionCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTransactionCategory()
    {
        $transactionCategory = $this->fakeTransactionCategoryData();
        $createdTransactionCategory = $this->transactionCategoryRepo->create($transactionCategory);
        $createdTransactionCategory = $createdTransactionCategory->toArray();
        $this->assertArrayHasKey('id', $createdTransactionCategory);
        $this->assertNotNull($createdTransactionCategory['id'], 'Created TransactionCategory must have id specified');
        $this->assertNotNull(TransactionCategory::find($createdTransactionCategory['id']), 'TransactionCategory with given id must be in DB');
        $this->assertModelData($transactionCategory, $createdTransactionCategory);
    }

    /**
     * @test read
     */
    public function testReadTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $dbTransactionCategory = $this->transactionCategoryRepo->find($transactionCategory->id);
        $dbTransactionCategory = $dbTransactionCategory->toArray();
        $this->assertModelData($transactionCategory->toArray(), $dbTransactionCategory);
    }

    /**
     * @test update
     */
    public function testUpdateTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $fakeTransactionCategory = $this->fakeTransactionCategoryData();
        $updatedTransactionCategory = $this->transactionCategoryRepo->update($fakeTransactionCategory, $transactionCategory->id);
        $this->assertModelData($fakeTransactionCategory, $updatedTransactionCategory->toArray());
        $dbTransactionCategory = $this->transactionCategoryRepo->find($transactionCategory->id);
        $this->assertModelData($fakeTransactionCategory, $dbTransactionCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $resp = $this->transactionCategoryRepo->delete($transactionCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(TransactionCategory::find($transactionCategory->id), 'TransactionCategory should not exist in DB');
    }
}
