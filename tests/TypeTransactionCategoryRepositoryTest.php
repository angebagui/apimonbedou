<?php

use App\Models\TypeTransactionCategory;
use App\Repositories\TypeTransactionCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeTransactionCategoryRepositoryTest extends TestCase
{
    use MakeTypeTransactionCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeTransactionCategoryRepository
     */
    protected $typeTransactionCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->typeTransactionCategoryRepo = App::make(TypeTransactionCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->fakeTypeTransactionCategoryData();
        $createdTypeTransactionCategory = $this->typeTransactionCategoryRepo->create($typeTransactionCategory);
        $createdTypeTransactionCategory = $createdTypeTransactionCategory->toArray();
        $this->assertArrayHasKey('id', $createdTypeTransactionCategory);
        $this->assertNotNull($createdTypeTransactionCategory['id'], 'Created TypeTransactionCategory must have id specified');
        $this->assertNotNull(TypeTransactionCategory::find($createdTypeTransactionCategory['id']), 'TypeTransactionCategory with given id must be in DB');
        $this->assertModelData($typeTransactionCategory, $createdTypeTransactionCategory);
    }

    /**
     * @test read
     */
    public function testReadTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $dbTypeTransactionCategory = $this->typeTransactionCategoryRepo->find($typeTransactionCategory->id);
        $dbTypeTransactionCategory = $dbTypeTransactionCategory->toArray();
        $this->assertModelData($typeTransactionCategory->toArray(), $dbTypeTransactionCategory);
    }

    /**
     * @test update
     */
    public function testUpdateTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $fakeTypeTransactionCategory = $this->fakeTypeTransactionCategoryData();
        $updatedTypeTransactionCategory = $this->typeTransactionCategoryRepo->update($fakeTypeTransactionCategory, $typeTransactionCategory->id);
        $this->assertModelData($fakeTypeTransactionCategory, $updatedTypeTransactionCategory->toArray());
        $dbTypeTransactionCategory = $this->typeTransactionCategoryRepo->find($typeTransactionCategory->id);
        $this->assertModelData($fakeTypeTransactionCategory, $dbTypeTransactionCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $resp = $this->typeTransactionCategoryRepo->delete($typeTransactionCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(TypeTransactionCategory::find($typeTransactionCategory->id), 'TypeTransactionCategory should not exist in DB');
    }
}
