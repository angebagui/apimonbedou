<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransactionCategoryApiTest extends TestCase
{
    use MakeTransactionCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTransactionCategory()
    {
        $transactionCategory = $this->fakeTransactionCategoryData();
        $this->json('POST', '/api/v1/transactionCategories', $transactionCategory);

        $this->assertApiResponse($transactionCategory);
    }

    /**
     * @test
     */
    public function testReadTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $this->json('GET', '/api/v1/transactionCategories/'.$transactionCategory->id);

        $this->assertApiResponse($transactionCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $editedTransactionCategory = $this->fakeTransactionCategoryData();

        $this->json('PUT', '/api/v1/transactionCategories/'.$transactionCategory->id, $editedTransactionCategory);

        $this->assertApiResponse($editedTransactionCategory);
    }

    /**
     * @test
     */
    public function testDeleteTransactionCategory()
    {
        $transactionCategory = $this->makeTransactionCategory();
        $this->json('DELETE', '/api/v1/transactionCategories/'.$transactionCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/transactionCategories/'.$transactionCategory->id);

        $this->assertResponseStatus(404);
    }
}
