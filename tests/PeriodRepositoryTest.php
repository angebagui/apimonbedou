<?php

use App\Models\Period;
use App\Repositories\PeriodRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PeriodRepositoryTest extends TestCase
{
    use MakePeriodTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeriodRepository
     */
    protected $periodRepo;

    public function setUp()
    {
        parent::setUp();
        $this->periodRepo = App::make(PeriodRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePeriod()
    {
        $period = $this->fakePeriodData();
        $createdPeriod = $this->periodRepo->create($period);
        $createdPeriod = $createdPeriod->toArray();
        $this->assertArrayHasKey('id', $createdPeriod);
        $this->assertNotNull($createdPeriod['id'], 'Created Period must have id specified');
        $this->assertNotNull(Period::find($createdPeriod['id']), 'Period with given id must be in DB');
        $this->assertModelData($period, $createdPeriod);
    }

    /**
     * @test read
     */
    public function testReadPeriod()
    {
        $period = $this->makePeriod();
        $dbPeriod = $this->periodRepo->find($period->id);
        $dbPeriod = $dbPeriod->toArray();
        $this->assertModelData($period->toArray(), $dbPeriod);
    }

    /**
     * @test update
     */
    public function testUpdatePeriod()
    {
        $period = $this->makePeriod();
        $fakePeriod = $this->fakePeriodData();
        $updatedPeriod = $this->periodRepo->update($fakePeriod, $period->id);
        $this->assertModelData($fakePeriod, $updatedPeriod->toArray());
        $dbPeriod = $this->periodRepo->find($period->id);
        $this->assertModelData($fakePeriod, $dbPeriod->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePeriod()
    {
        $period = $this->makePeriod();
        $resp = $this->periodRepo->delete($period->id);
        $this->assertTrue($resp);
        $this->assertNull(Period::find($period->id), 'Period should not exist in DB');
    }
}
