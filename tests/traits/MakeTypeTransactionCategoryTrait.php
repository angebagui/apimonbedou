<?php

use Faker\Factory as Faker;
use App\Models\TypeTransactionCategory;
use App\Repositories\TypeTransactionCategoryRepository;

trait MakeTypeTransactionCategoryTrait
{
    /**
     * Create fake instance of TypeTransactionCategory and save it in database
     *
     * @param array $typeTransactionCategoryFields
     * @return TypeTransactionCategory
     */
    public function makeTypeTransactionCategory($typeTransactionCategoryFields = [])
    {
        /** @var TypeTransactionCategoryRepository $typeTransactionCategoryRepo */
        $typeTransactionCategoryRepo = App::make(TypeTransactionCategoryRepository::class);
        $theme = $this->fakeTypeTransactionCategoryData($typeTransactionCategoryFields);
        return $typeTransactionCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of TypeTransactionCategory
     *
     * @param array $typeTransactionCategoryFields
     * @return TypeTransactionCategory
     */
    public function fakeTypeTransactionCategory($typeTransactionCategoryFields = [])
    {
        return new TypeTransactionCategory($this->fakeTypeTransactionCategoryData($typeTransactionCategoryFields));
    }

    /**
     * Get fake data of TypeTransactionCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTypeTransactionCategoryData($typeTransactionCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $typeTransactionCategoryFields);
    }
}
