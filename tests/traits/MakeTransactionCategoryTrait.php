<?php

use Faker\Factory as Faker;
use App\Models\TransactionCategory;
use App\Repositories\TransactionCategoryRepository;

trait MakeTransactionCategoryTrait
{
    /**
     * Create fake instance of TransactionCategory and save it in database
     *
     * @param array $transactionCategoryFields
     * @return TransactionCategory
     */
    public function makeTransactionCategory($transactionCategoryFields = [])
    {
        /** @var TransactionCategoryRepository $transactionCategoryRepo */
        $transactionCategoryRepo = App::make(TransactionCategoryRepository::class);
        $theme = $this->fakeTransactionCategoryData($transactionCategoryFields);
        return $transactionCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of TransactionCategory
     *
     * @param array $transactionCategoryFields
     * @return TransactionCategory
     */
    public function fakeTransactionCategory($transactionCategoryFields = [])
    {
        return new TransactionCategory($this->fakeTransactionCategoryData($transactionCategoryFields));
    }

    /**
     * Get fake data of TransactionCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTransactionCategoryData($transactionCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->randomDigitNotNull,
            'type_category_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'language' => $fake->word,
            'logo' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $transactionCategoryFields);
    }
}
