<?php

use Faker\Factory as Faker;
use App\Models\Period;
use App\Repositories\PeriodRepository;

trait MakePeriodTrait
{
    /**
     * Create fake instance of Period and save it in database
     *
     * @param array $periodFields
     * @return Period
     */
    public function makePeriod($periodFields = [])
    {
        /** @var PeriodRepository $periodRepo */
        $periodRepo = App::make(PeriodRepository::class);
        $theme = $this->fakePeriodData($periodFields);
        return $periodRepo->create($theme);
    }

    /**
     * Get fake instance of Period
     *
     * @param array $periodFields
     * @return Period
     */
    public function fakePeriod($periodFields = [])
    {
        return new Period($this->fakePeriodData($periodFields));
    }

    /**
     * Get fake data of Period
     *
     * @param array $postFields
     * @return array
     */
    public function fakePeriodData($periodFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $periodFields);
    }
}
