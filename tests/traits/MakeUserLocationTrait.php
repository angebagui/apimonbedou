<?php

use Faker\Factory as Faker;
use App\Models\UserLocation;
use App\Repositories\UserLocationRepository;

trait MakeUserLocationTrait
{
    /**
     * Create fake instance of UserLocation and save it in database
     *
     * @param array $userLocationFields
     * @return UserLocation
     */
    public function makeUserLocation($userLocationFields = [])
    {
        /** @var UserLocationRepository $userLocationRepo */
        $userLocationRepo = App::make(UserLocationRepository::class);
        $theme = $this->fakeUserLocationData($userLocationFields);
        return $userLocationRepo->create($theme);
    }

    /**
     * Get fake instance of UserLocation
     *
     * @param array $userLocationFields
     * @return UserLocation
     */
    public function fakeUserLocation($userLocationFields = [])
    {
        return new UserLocation($this->fakeUserLocationData($userLocationFields));
    }

    /**
     * Get fake data of UserLocation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUserLocationData($userLocationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'client_id' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'location_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $userLocationFields);
    }
}
