<?php

use Faker\Factory as Faker;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;

trait MakeTransactionTrait
{
    /**
     * Create fake instance of Transaction and save it in database
     *
     * @param array $transactionFields
     * @return Transaction
     */
    public function makeTransaction($transactionFields = [])
    {
        /** @var TransactionRepository $transactionRepo */
        $transactionRepo = App::make(TransactionRepository::class);
        $theme = $this->fakeTransactionData($transactionFields);
        return $transactionRepo->create($theme);
    }

    /**
     * Get fake instance of Transaction
     *
     * @param array $transactionFields
     * @return Transaction
     */
    public function fakeTransaction($transactionFields = [])
    {
        return new Transaction($this->fakeTransactionData($transactionFields));
    }

    /**
     * Get fake data of Transaction
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTransactionData($transactionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'client_id' => $fake->word,
            'transaction_category_id' => $fake->randomDigitNotNull,
            'wallet_id' => $fake->randomDigitNotNull,
            'amount' => $fake->word,
            'currency' => $fake->word,
            'note' => $fake->word,
            'location_lat' => $fake->randomDigitNotNull,
            'location_lng' => $fake->randomDigitNotNull,
            'location_address' => $fake->word,
            'location_google_ref' => $fake->word,
            'picture' => $fake->word,
            'is_repeated' => $fake->word,
            'repeated_by' => $fake->randomDigitNotNull,
            'done_at' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $transactionFields);
    }
}
