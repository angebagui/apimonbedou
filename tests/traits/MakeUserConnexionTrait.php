<?php

use Faker\Factory as Faker;
use App\Models\UserConnexion;
use App\Repositories\UserConnexionRepository;

trait MakeUserConnexionTrait
{
    /**
     * Create fake instance of UserConnexion and save it in database
     *
     * @param array $userConnexionFields
     * @return UserConnexion
     */
    public function makeUserConnexion($userConnexionFields = [])
    {
        /** @var UserConnexionRepository $userConnexionRepo */
        $userConnexionRepo = App::make(UserConnexionRepository::class);
        $theme = $this->fakeUserConnexionData($userConnexionFields);
        return $userConnexionRepo->create($theme);
    }

    /**
     * Get fake instance of UserConnexion
     *
     * @param array $userConnexionFields
     * @return UserConnexion
     */
    public function fakeUserConnexion($userConnexionFields = [])
    {
        return new UserConnexion($this->fakeUserConnexionData($userConnexionFields));
    }

    /**
     * Get fake data of UserConnexion
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUserConnexionData($userConnexionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'device_model' => $fake->word,
            'device_os' => $fake->word,
            'device_id' => $fake->word,
            'device_info' => $fake->word,
            'network' => $fake->word,
            'latitude' => $fake->word,
            'longitude' => $fake->word,
            'success' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $userConnexionFields);
    }
}
