<?php

use Faker\Factory as Faker;
use App\Models\Wallet;
use App\Repositories\WalletRepository;

trait MakeWalletTrait
{
    /**
     * Create fake instance of Wallet and save it in database
     *
     * @param array $walletFields
     * @return Wallet
     */
    public function makeWallet($walletFields = [])
    {
        /** @var WalletRepository $walletRepo */
        $walletRepo = App::make(WalletRepository::class);
        $theme = $this->fakeWalletData($walletFields);
        return $walletRepo->create($theme);
    }

    /**
     * Get fake instance of Wallet
     *
     * @param array $walletFields
     * @return Wallet
     */
    public function fakeWallet($walletFields = [])
    {
        return new Wallet($this->fakeWalletData($walletFields));
    }

    /**
     * Get fake data of Wallet
     *
     * @param array $postFields
     * @return array
     */
    public function fakeWalletData($walletFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'client_id' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'init_balance' => $fake->word,
            'currency' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $walletFields);
    }
}
