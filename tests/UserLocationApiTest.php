<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserLocationApiTest extends TestCase
{
    use MakeUserLocationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUserLocation()
    {
        $userLocation = $this->fakeUserLocationData();
        $this->json('POST', '/api/v1/userLocations', $userLocation);

        $this->assertApiResponse($userLocation);
    }

    /**
     * @test
     */
    public function testReadUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $this->json('GET', '/api/v1/userLocations/'.$userLocation->id);

        $this->assertApiResponse($userLocation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $editedUserLocation = $this->fakeUserLocationData();

        $this->json('PUT', '/api/v1/userLocations/'.$userLocation->id, $editedUserLocation);

        $this->assertApiResponse($editedUserLocation);
    }

    /**
     * @test
     */
    public function testDeleteUserLocation()
    {
        $userLocation = $this->makeUserLocation();
        $this->json('DELETE', '/api/v1/userLocations/'.$userLocation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/userLocations/'.$userLocation->id);

        $this->assertResponseStatus(404);
    }
}
