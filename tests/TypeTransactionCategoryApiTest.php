<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeTransactionCategoryApiTest extends TestCase
{
    use MakeTypeTransactionCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->fakeTypeTransactionCategoryData();
        $this->json('POST', '/api/v1/typeTransactionCategories', $typeTransactionCategory);

        $this->assertApiResponse($typeTransactionCategory);
    }

    /**
     * @test
     */
    public function testReadTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $this->json('GET', '/api/v1/typeTransactionCategories/'.$typeTransactionCategory->id);

        $this->assertApiResponse($typeTransactionCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $editedTypeTransactionCategory = $this->fakeTypeTransactionCategoryData();

        $this->json('PUT', '/api/v1/typeTransactionCategories/'.$typeTransactionCategory->id, $editedTypeTransactionCategory);

        $this->assertApiResponse($editedTypeTransactionCategory);
    }

    /**
     * @test
     */
    public function testDeleteTypeTransactionCategory()
    {
        $typeTransactionCategory = $this->makeTypeTransactionCategory();
        $this->json('DELETE', '/api/v1/typeTransactionCategories/'.$typeTransactionCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/typeTransactionCategories/'.$typeTransactionCategory->id);

        $this->assertResponseStatus(404);
    }
}
