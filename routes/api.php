<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function () {
   
   	Route::resource('users', 'UserAPIController');

   	Route::post('users/login', 'UserAPIController@authenticate');

	Route::resource('userlocations', 'UserLocationAPIController');

	Route::resource('wallets', 'WalletAPIController');

	Route::resource('locations', 'LocationAPIController');

	Route::resource('typetransactioncategories', 'TypeTransactionCategoryAPIController');

	Route::resource('transactioncategories', 'TransactionCategoryAPIController');

	Route::resource('budgets', 'BudgetAPIController');

	Route::resource('transactions', 'TransactionAPIController');

	Route::resource('periods', 'PeriodAPIController');

	Route::resource('userconnexions', 'UserConnexionAPIController');


});

/*Route::domain('{account}.myapp.com')->group(function () {
    Route::get('user/{id}', function ($account, $id) {
        
    });
});*/

