<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="UserConnexion",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="device_model",
 *          description="device_model",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_manufacturer",
 *          description="device_manufacturer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_product",
 *          description="device_product",
 *          type="string"
 *      ), 
 *      @SWG\Property(
 *          property="device_os_id",
 *          description="device_os_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_os_version_id",
 *          description="device_os_version_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_os_version_name",
 *          description="device_os_version_name",
 *          type="string"
 *      ), 
 *      @SWG\Property(
 *          property="device_id",
 *          description="device_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_info",
 *          description="device_info",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="application_name",
 *          description="application_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="application_version_name",
 *          description="application_version_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="application_version_code",
 *          description="application_version_code",
 *          type="string"
 *      ), 
 *      @SWG\Property(
 *          property="screen_width",
 *          description="screen_width",
 *          type="string"
 *      ), 
 *      @SWG\Property(
 *          property="screen_height",
 *          description="screen_height",
 *          type="string"
 *      ), 
 *      @SWG\Property(
 *          property="screen_density",
 *          description="screen_density",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="screen_density_name",
 *          description="screen_density_name",
 *          type="string"
 *      ),    
 *      @SWG\Property(
 *          property="network",
 *          description="network",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="success",
 *          description="success",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class UserConnexion extends Model
{
    use SoftDeletes;

    public $table = 'user_connexions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'device_model',
        'device_manufacturer',
        'device_product',
        'device_os_id',
        'device_os_version_id',
        'device_os_version_name',
        'device_id',
        'device_info',
        'application_name',
        'application_version_name',
        'application_version_code',
        'screen_width',
        'screen_height',
        'screen_density',
        'screen_density_name',
        'network',
        'latitude',
        'longitude',
        'success'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [  
        'device_model' => 'string',
        'device_manufacturer' => 'string',
        'device_product' => 'string',
        'device_os_id' => 'string',
        'device_os_version_id' => 'string',
        'device_os_version_name' => 'string',
        'device_id' => 'string',
        'device_info' => 'string',
        'application_name' => 'string',
        'application_version_name' => 'string',
        'application_version_code' => 'string',
        'screen_width' => 'string',
        'screen_height' => 'string',
        'screen_density' => 'string',
        'screen_density_name' => 'string',
        'network' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'success' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
