<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transaction_category_id",
 *          description="transaction_category_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="wallet_id",
 *          description="wallet_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency",
 *          description="currency",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lat",
 *          description="location_lat",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="location_lng",
 *          description="location_lng",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="location_address",
 *          description="location_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_google_ref",
 *          description="location_google_ref",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture",
 *          description="picture",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_repeated",
 *          description="is_repeated",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="repeated_by",
 *          description="repeated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="done_at",
 *          description="done_at",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transactions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'client_id',
        'transaction_category_id',
        'wallet_id',
        'amount',
        'currency',
        'note',
        'location_lat',
        'location_lng',
        'location_address',
        'location_google_ref',
        'picture',
        'is_repeated',
        'repeated_by',
        'done_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'string',
        'transaction_category_id' => 'integer',
        'wallet_id' => 'integer',
        'amount' => 'string',
        'currency' => 'string',
        'note' => 'string',
        'location_lat' => 'integer',
        'location_lng' => 'integer',
        'location_address' => 'string',
        'location_google_ref' => 'string',
        'picture' => 'string',
        'is_repeated' => 'boolean',
        'repeated_by' => 'integer',
        'done_at' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
