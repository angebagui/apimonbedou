<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeTransactionCategoryRequest;
use App\Http\Requests\UpdateTypeTransactionCategoryRequest;
use App\Repositories\TypeTransactionCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TypeTransactionCategoryController extends AppBaseController
{
    /** @var  TypeTransactionCategoryRepository */
    private $typeTransactionCategoryRepository;

    public function __construct(TypeTransactionCategoryRepository $typeTransactionCategoryRepo)
    {
        $this->typeTransactionCategoryRepository = $typeTransactionCategoryRepo;
    }

    /**
     * Display a listing of the TypeTransactionCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->typeTransactionCategoryRepository->pushCriteria(new RequestCriteria($request));
        $typeTransactionCategories = $this->typeTransactionCategoryRepository->all();

        return view('type_transaction_categories.index')
            ->with('typeTransactionCategories', $typeTransactionCategories);
    }

    /**
     * Show the form for creating a new TypeTransactionCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_transaction_categories.create');
    }

    /**
     * Store a newly created TypeTransactionCategory in storage.
     *
     * @param CreateTypeTransactionCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeTransactionCategoryRequest $request)
    {
        $input = $request->all();

        $typeTransactionCategory = $this->typeTransactionCategoryRepository->create($input);

        Flash::success('Type Transaction Category saved successfully.');

        return redirect(route('typeTransactionCategories.index'));
    }

    /**
     * Display the specified TypeTransactionCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            Flash::error('Type Transaction Category not found');

            return redirect(route('typeTransactionCategories.index'));
        }

        return view('type_transaction_categories.show')->with('typeTransactionCategory', $typeTransactionCategory);
    }

    /**
     * Show the form for editing the specified TypeTransactionCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            Flash::error('Type Transaction Category not found');

            return redirect(route('typeTransactionCategories.index'));
        }

        return view('type_transaction_categories.edit')->with('typeTransactionCategory', $typeTransactionCategory);
    }

    /**
     * Update the specified TypeTransactionCategory in storage.
     *
     * @param  int              $id
     * @param UpdateTypeTransactionCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeTransactionCategoryRequest $request)
    {
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            Flash::error('Type Transaction Category not found');

            return redirect(route('typeTransactionCategories.index'));
        }

        $typeTransactionCategory = $this->typeTransactionCategoryRepository->update($request->all(), $id);

        Flash::success('Type Transaction Category updated successfully.');

        return redirect(route('typeTransactionCategories.index'));
    }

    /**
     * Remove the specified TypeTransactionCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            Flash::error('Type Transaction Category not found');

            return redirect(route('typeTransactionCategories.index'));
        }

        $this->typeTransactionCategoryRepository->delete($id);

        Flash::success('Type Transaction Category deleted successfully.');

        return redirect(route('typeTransactionCategories.index'));
    }
}
