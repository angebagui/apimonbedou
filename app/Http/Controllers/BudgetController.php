<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBudgetRequest;
use App\Http\Requests\UpdateBudgetRequest;
use App\Repositories\BudgetRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BudgetController extends AppBaseController
{
    /** @var  BudgetRepository */
    private $budgetRepository;

    public function __construct(BudgetRepository $budgetRepo)
    {
        $this->budgetRepository = $budgetRepo;
    }

    /**
     * Display a listing of the Budget.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->budgetRepository->pushCriteria(new RequestCriteria($request));
        $budgets = $this->budgetRepository->all();

        return view('budgets.index')
            ->with('budgets', $budgets);
    }

    /**
     * Show the form for creating a new Budget.
     *
     * @return Response
     */
    public function create()
    {
        return view('budgets.create');
    }

    /**
     * Store a newly created Budget in storage.
     *
     * @param CreateBudgetRequest $request
     *
     * @return Response
     */
    public function store(CreateBudgetRequest $request)
    {
        $input = $request->all();

        $budget = $this->budgetRepository->create($input);

        Flash::success('Budget saved successfully.');

        return redirect(route('budgets.index'));
    }

    /**
     * Display the specified Budget.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            Flash::error('Budget not found');

            return redirect(route('budgets.index'));
        }

        return view('budgets.show')->with('budget', $budget);
    }

    /**
     * Show the form for editing the specified Budget.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            Flash::error('Budget not found');

            return redirect(route('budgets.index'));
        }

        return view('budgets.edit')->with('budget', $budget);
    }

    /**
     * Update the specified Budget in storage.
     *
     * @param  int              $id
     * @param UpdateBudgetRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBudgetRequest $request)
    {
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            Flash::error('Budget not found');

            return redirect(route('budgets.index'));
        }

        $budget = $this->budgetRepository->update($request->all(), $id);

        Flash::success('Budget updated successfully.');

        return redirect(route('budgets.index'));
    }

    /**
     * Remove the specified Budget from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            Flash::error('Budget not found');

            return redirect(route('budgets.index'));
        }

        $this->budgetRepository->delete($id);

        Flash::success('Budget deleted successfully.');

        return redirect(route('budgets.index'));
    }
}
