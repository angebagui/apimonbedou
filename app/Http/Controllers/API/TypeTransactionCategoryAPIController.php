<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeTransactionCategoryAPIRequest;
use App\Http\Requests\API\UpdateTypeTransactionCategoryAPIRequest;
use App\Models\TypeTransactionCategory;
use App\Repositories\TypeTransactionCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeTransactionCategoryController
 * @package App\Http\Controllers\API
 */

class TypeTransactionCategoryAPIController extends AppBaseController
{
    /** @var  TypeTransactionCategoryRepository */
    private $typeTransactionCategoryRepository;

    public function __construct(TypeTransactionCategoryRepository $typeTransactionCategoryRepo)
    {
        $this->typeTransactionCategoryRepository = $typeTransactionCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/typetransactioncategories",
     *      summary="Get a listing of the TypeTransactionCategories.",
     *      tags={"TypeTransactionCategory"},
     *      description="Get all TypeTransactionCategories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TypeTransactionCategory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->typeTransactionCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->typeTransactionCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeTransactionCategories = $this->typeTransactionCategoryRepository->all();

        return $this->sendResponse($typeTransactionCategories->toArray(), 'Type Transaction Categories retrieved successfully');
    }

    /**
     * @param CreateTypeTransactionCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/typetransactioncategories",
     *      summary="Store a newly created TypeTransactionCategory in storage",
     *      tags={"TypeTransactionCategory"},
     *      description="Store TypeTransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeTransactionCategory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeTransactionCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTypeTransactionCategoryAPIRequest $request)
    {
        $input = $request->json()->all();

        $typeTransactionCategories = $this->typeTransactionCategoryRepository->create($input);

        return $this->sendResponse($typeTransactionCategories->toArray(), 'Type Transaction Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/typetransactioncategories/{id}",
     *      summary="Display the specified TypeTransactionCategory",
     *      tags={"TypeTransactionCategory"},
     *      description="Get TypeTransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TypeTransactionCategory $typeTransactionCategory */
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            return $this->sendError('Type Transaction Category not found');
        }

        return $this->sendResponse($typeTransactionCategory->toArray(), 'Type Transaction Category retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTypeTransactionCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/typetransactioncategories/{id}",
     *      summary="Update the specified TypeTransactionCategory in storage",
     *      tags={"TypeTransactionCategory"},
     *      description="Update TypeTransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeTransactionCategory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeTransactionCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTypeTransactionCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeTransactionCategory $typeTransactionCategory */
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            return $this->sendError('Type Transaction Category not found');
        }

        $typeTransactionCategory = $this->typeTransactionCategoryRepository->update($input, $id);

        return $this->sendResponse($typeTransactionCategory->toArray(), 'TypeTransactionCategory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/typetransactioncategories/{id}",
     *      summary="Remove the specified TypeTransactionCategory from storage",
     *      tags={"TypeTransactionCategory"},
     *      description="Delete TypeTransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TypeTransactionCategory $typeTransactionCategory */
        $typeTransactionCategory = $this->typeTransactionCategoryRepository->findWithoutFail($id);

        if (empty($typeTransactionCategory)) {
            return $this->sendError('Type Transaction Category not found');
        }

        $typeTransactionCategory->delete();

        return $this->sendResponse($id, 'Type Transaction Category deleted successfully');
    }
}
