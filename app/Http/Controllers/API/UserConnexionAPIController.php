<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserConnexionAPIRequest;
use App\Http\Requests\API\UpdateUserConnexionAPIRequest;
use App\Models\UserConnexion;
use App\Repositories\UserConnexionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserConnexionController
 * @package App\Http\Controllers\API
 */

class UserConnexionAPIController extends AppBaseController
{
    /** @var  UserConnexionRepository */
    private $userConnexionRepository;

    public function __construct(UserConnexionRepository $userConnexionRepo)
    {
        $this->userConnexionRepository = $userConnexionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/userconnexions",
     *      summary="Get a listing of the UserConnexions.",
     *      tags={"UserConnexion"},
     *      description="Get all UserConnexions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserConnexion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->userConnexionRepository->pushCriteria(new RequestCriteria($request));
        $this->userConnexionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $userConnexions = $this->userConnexionRepository->all();

        return $this->sendResponse($userConnexions->toArray(), 'User Connexions retrieved successfully');
    }

    /**
     * @param CreateUserConnexionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/userconnexions",
     *      summary="Store a newly created UserConnexion in storage",
     *      tags={"UserConnexion"},
     *      description="Store UserConnexion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserConnexion that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserConnexion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserConnexion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUserConnexionAPIRequest $request)
    {
        $input = $request->json()->all();

        $userConnexions = $this->userConnexionRepository->create($input);

        return $this->sendResponse($userConnexions->toArray(), 'User Connexion saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/userconnexions/{id}",
     *      summary="Display the specified UserConnexion",
     *      tags={"UserConnexion"},
     *      description="Get UserConnexion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserConnexion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserConnexion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UserConnexion $userConnexion */
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            return $this->sendError('User Connexion not found');
        }

        return $this->sendResponse($userConnexion->toArray(), 'User Connexion retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUserConnexionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/userconnexions/{id}",
     *      summary="Update the specified UserConnexion in storage",
     *      tags={"UserConnexion"},
     *      description="Update UserConnexion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserConnexion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserConnexion that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserConnexion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserConnexion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUserConnexionAPIRequest $request)
    {
        $input = $request->json()->all();

        /** @var UserConnexion $userConnexion */
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            return $this->sendError('User Connexion not found');
        }

        $userConnexion = $this->userConnexionRepository->update($input, $id);

        return $this->sendResponse($userConnexion->toArray(), 'UserConnexion updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/userconnexions/{id}",
     *      summary="Remove the specified UserConnexion from storage",
     *      tags={"UserConnexion"},
     *      description="Delete UserConnexion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserConnexion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UserConnexion $userConnexion */
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            return $this->sendError('User Connexion not found');
        }

        $userConnexion->delete();

        return $this->sendResponse($id, 'User Connexion deleted successfully');
    }
}
