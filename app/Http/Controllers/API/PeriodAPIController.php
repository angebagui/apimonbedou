<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePeriodAPIRequest;
use App\Http\Requests\API\UpdatePeriodAPIRequest;
use App\Models\Period;
use App\Repositories\PeriodRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PeriodController
 * @package App\Http\Controllers\API
 */

class PeriodAPIController extends AppBaseController
{
    /** @var  PeriodRepository */
    private $periodRepository;

    public function __construct(PeriodRepository $periodRepo)
    {
        $this->periodRepository = $periodRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/periods",
     *      summary="Get a listing of the Periods.",
     *      tags={"Period"},
     *      description="Get all Periods",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Period")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->periodRepository->pushCriteria(new RequestCriteria($request));
        $this->periodRepository->pushCriteria(new LimitOffsetCriteria($request));
        $periods = $this->periodRepository->all();

        return $this->sendResponse($periods->toArray(), 'Periods retrieved successfully');
    }

    /**
     * @param CreatePeriodAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/periods",
     *      summary="Store a newly created Period in storage",
     *      tags={"Period"},
     *      description="Store Period",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Period that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Period")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Period"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePeriodAPIRequest $request)
    {
        $input = $request->json()->all();

        $periods = $this->periodRepository->create($input);

        return $this->sendResponse($periods->toArray(), 'Period saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/periods/{id}",
     *      summary="Display the specified Period",
     *      tags={"Period"},
     *      description="Get Period",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Period",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Period"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Period $period */
        $period = $this->periodRepository->findWithoutFail($id);

        if (empty($period)) {
            return $this->sendError('Period not found');
        }

        return $this->sendResponse($period->toArray(), 'Period retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePeriodAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/periods/{id}",
     *      summary="Update the specified Period in storage",
     *      tags={"Period"},
     *      description="Update Period",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Period",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Period that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Period")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Period"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePeriodAPIRequest $request)
    {
        $input = $request->json()->all();

        /** @var Period $period */
        $period = $this->periodRepository->findWithoutFail($id);

        if (empty($period)) {
            return $this->sendError('Period not found');
        }

        $period = $this->periodRepository->update($input, $id);

        return $this->sendResponse($period->toArray(), 'Period updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/periods/{id}",
     *      summary="Remove the specified Period from storage",
     *      tags={"Period"},
     *      description="Delete Period",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Period",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Period $period */
        $period = $this->periodRepository->findWithoutFail($id);

        if (empty($period)) {
            return $this->sendError('Period not found');
        }

        $period->delete();

        return $this->sendResponse($id, 'Period deleted successfully');
    }
}
