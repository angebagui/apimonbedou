<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTransactionCategoryAPIRequest;
use App\Http\Requests\API\UpdateTransactionCategoryAPIRequest;
use App\Models\TransactionCategory;
use App\Repositories\TransactionCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TransactionCategoryController
 * @package App\Http\Controllers\API
 */

class TransactionCategoryAPIController extends AppBaseController
{
    /** @var  TransactionCategoryRepository */
    private $transactionCategoryRepository;

    public function __construct(TransactionCategoryRepository $transactionCategoryRepo)
    {
        $this->transactionCategoryRepository = $transactionCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/transactioncategories",
     *      summary="Get a listing of the TransactionCategories.",
     *      tags={"TransactionCategory"},
     *      description="Get all TransactionCategories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TransactionCategory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->transactionCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->transactionCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $transactionCategories = $this->transactionCategoryRepository->all();

        return $this->sendResponse($transactionCategories->toArray(), 'Transaction Categories retrieved successfully');
    }

    /**
     * @param CreateTransactionCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/transactioncategories",
     *      summary="Store a newly created TransactionCategory in storage",
     *      tags={"TransactionCategory"},
     *      description="Store TransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TransactionCategory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TransactionCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTransactionCategoryAPIRequest $request)
    {
        $input = $request->json()->all();

        $transactionCategories = $this->transactionCategoryRepository->create($input);

        return $this->sendResponse($transactionCategories->toArray(), 'Transaction Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/transactioncategories/{id}",
     *      summary="Display the specified TransactionCategory",
     *      tags={"TransactionCategory"},
     *      description="Get TransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TransactionCategory $transactionCategory */
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            return $this->sendError('Transaction Category not found');
        }

        return $this->sendResponse($transactionCategory->toArray(), 'Transaction Category retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTransactionCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/transactioncategories/{id}",
     *      summary="Update the specified TransactionCategory in storage",
     *      tags={"TransactionCategory"},
     *      description="Update TransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TransactionCategory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TransactionCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TransactionCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTransactionCategoryAPIRequest $request)
    {
        $input = $request->json()->all();

        /** @var TransactionCategory $transactionCategory */
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            return $this->sendError('Transaction Category not found');
        }

        $transactionCategory = $this->transactionCategoryRepository->update($input, $id);

        return $this->sendResponse($transactionCategory->toArray(), 'TransactionCategory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/transactioncategories/{id}",
     *      summary="Remove the specified TransactionCategory from storage",
     *      tags={"TransactionCategory"},
     *      description="Delete TransactionCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TransactionCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TransactionCategory $transactionCategory */
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            return $this->sendError('Transaction Category not found');
        }

        $transactionCategory->delete();

        return $this->sendResponse($id, 'Transaction Category deleted successfully');
    }
}
