<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserLocationAPIRequest;
use App\Http\Requests\API\UpdateUserLocationAPIRequest;
use App\Models\UserLocation;
use App\Repositories\UserLocationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserLocationController
 * @package App\Http\Controllers\API
 */

class UserLocationAPIController extends AppBaseController
{
    /** @var  UserLocationRepository */
    private $userLocationRepository;

    public function __construct(UserLocationRepository $userLocationRepo)
    {
        $this->userLocationRepository = $userLocationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/userlocations",
     *      summary="Get a listing of the UserLocations.",
     *      tags={"UserLocation"},
     *      description="Get all UserLocations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserLocation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->userLocationRepository->pushCriteria(new RequestCriteria($request));
        $this->userLocationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $userLocations = $this->userLocationRepository->all();

        return $this->sendResponse($userLocations->toArray(), 'User Locations retrieved successfully');
    }

    /**
     * @param CreateUserLocationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/userlocations",
     *      summary="Store a newly created UserLocation in storage",
     *      tags={"UserLocation"},
     *      description="Store UserLocation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserLocation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserLocation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserLocation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUserLocationAPIRequest $request)
    {
        $input = $request->json()->all();

        $userLocations = $this->userLocationRepository->create($input);

        return $this->sendResponse($userLocations->toArray(), 'User Location saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/userlocations/{id}",
     *      summary="Display the specified UserLocation",
     *      tags={"UserLocation"},
     *      description="Get UserLocation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserLocation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserLocation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UserLocation $userLocation */
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            return $this->sendError('User Location not found');
        }

        return $this->sendResponse($userLocation->toArray(), 'User Location retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUserLocationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/userlocations/{id}",
     *      summary="Update the specified UserLocation in storage",
     *      tags={"UserLocation"},
     *      description="Update UserLocation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserLocation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserLocation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserLocation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserLocation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUserLocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var UserLocation $userLocation */
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            return $this->sendError('User Location not found');
        }

        $userLocation = $this->userLocationRepository->update($input, $id);

        return $this->sendResponse($userLocation->toArray(), 'UserLocation updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/userlocations/{id}",
     *      summary="Remove the specified UserLocation from storage",
     *      tags={"UserLocation"},
     *      description="Delete UserLocation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserLocation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UserLocation $userLocation */
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            return $this->sendError('User Location not found');
        }

        $userLocation->delete();

        return $this->sendResponse($id, 'User Location deleted successfully');
    }
}
