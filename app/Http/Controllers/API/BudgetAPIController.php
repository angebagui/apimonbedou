<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBudgetAPIRequest;
use App\Http\Requests\API\UpdateBudgetAPIRequest;
use App\Models\Budget;
use App\Repositories\BudgetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BudgetController
 * @package App\Http\Controllers\API
 */

class BudgetAPIController extends AppBaseController
{
    /** @var  BudgetRepository */
    private $budgetRepository;

    public function __construct(BudgetRepository $budgetRepo)
    {
        $this->budgetRepository = $budgetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/budgets",
     *      summary="Get a listing of the Budgets.",
     *      tags={"Budget"},
     *      description="Get all Budgets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Budget")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->budgetRepository->pushCriteria(new RequestCriteria($request));
        $this->budgetRepository->pushCriteria(new LimitOffsetCriteria($request));
        $budgets = $this->budgetRepository->all();

        return $this->sendResponse($budgets->toArray(), 'Budgets retrieved successfully');
    }

    /**
     * @param CreateBudgetAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/budgets",
     *      summary="Store a newly created Budget in storage",
     *      tags={"Budget"},
     *      description="Store Budget",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Budget that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Budget")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Budget"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBudgetAPIRequest $request)
    {
        $input = $request->json()->all();

        $budgets = $this->budgetRepository->create($input);

        return $this->sendResponse($budgets->toArray(), 'Budget saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/budgets/{id}",
     *      summary="Display the specified Budget",
     *      tags={"Budget"},
     *      description="Get Budget",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Budget",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Budget"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Budget $budget */
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            return $this->sendError('Budget not found');
        }

        return $this->sendResponse($budget->toArray(), 'Budget retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBudgetAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/budgets/{id}",
     *      summary="Update the specified Budget in storage",
     *      tags={"Budget"},
     *      description="Update Budget",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Budget",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Budget that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Budget")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Budget"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBudgetAPIRequest $request)
    {
        $input = $request->json()->all();

        /** @var Budget $budget */
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            return $this->sendError('Budget not found');
        }

        $budget = $this->budgetRepository->update($input, $id);

        return $this->sendResponse($budget->toArray(), 'Budget updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/budgets/{id}",
     *      summary="Remove the specified Budget from storage",
     *      tags={"Budget"},
     *      description="Delete Budget",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Budget",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Budget $budget */
        $budget = $this->budgetRepository->findWithoutFail($id);

        if (empty($budget)) {
            return $this->sendError('Budget not found');
        }

        $budget->delete();

        return $this->sendResponse($id, 'Budget deleted successfully');
    }
}
