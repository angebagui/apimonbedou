<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserLocationRequest;
use App\Http\Requests\UpdateUserLocationRequest;
use App\Repositories\UserLocationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserLocationController extends AppBaseController
{
    /** @var  UserLocationRepository */
    private $userLocationRepository;

    public function __construct(UserLocationRepository $userLocationRepo)
    {
        $this->userLocationRepository = $userLocationRepo;
    }

    /**
     * Display a listing of the UserLocation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userLocationRepository->pushCriteria(new RequestCriteria($request));
        $userLocations = $this->userLocationRepository->all();

        return view('user_locations.index')
            ->with('userLocations', $userLocations);
    }

    /**
     * Show the form for creating a new UserLocation.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_locations.create');
    }

    /**
     * Store a newly created UserLocation in storage.
     *
     * @param CreateUserLocationRequest $request
     *
     * @return Response
     */
    public function store(CreateUserLocationRequest $request)
    {
        $input = $request->all();

        $userLocation = $this->userLocationRepository->create($input);

        Flash::success('User Location saved successfully.');

        return redirect(route('userLocations.index'));
    }

    /**
     * Display the specified UserLocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            Flash::error('User Location not found');

            return redirect(route('userLocations.index'));
        }

        return view('user_locations.show')->with('userLocation', $userLocation);
    }

    /**
     * Show the form for editing the specified UserLocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            Flash::error('User Location not found');

            return redirect(route('userLocations.index'));
        }

        return view('user_locations.edit')->with('userLocation', $userLocation);
    }

    /**
     * Update the specified UserLocation in storage.
     *
     * @param  int              $id
     * @param UpdateUserLocationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserLocationRequest $request)
    {
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            Flash::error('User Location not found');

            return redirect(route('userLocations.index'));
        }

        $userLocation = $this->userLocationRepository->update($request->all(), $id);

        Flash::success('User Location updated successfully.');

        return redirect(route('userLocations.index'));
    }

    /**
     * Remove the specified UserLocation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userLocation = $this->userLocationRepository->findWithoutFail($id);

        if (empty($userLocation)) {
            Flash::error('User Location not found');

            return redirect(route('userLocations.index'));
        }

        $this->userLocationRepository->delete($id);

        Flash::success('User Location deleted successfully.');

        return redirect(route('userLocations.index'));
    }
}
