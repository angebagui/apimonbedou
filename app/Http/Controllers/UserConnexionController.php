<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserConnexionRequest;
use App\Http\Requests\UpdateUserConnexionRequest;
use App\Repositories\UserConnexionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserConnexionController extends AppBaseController
{
    /** @var  UserConnexionRepository */
    private $userConnexionRepository;

    public function __construct(UserConnexionRepository $userConnexionRepo)
    {
        $this->userConnexionRepository = $userConnexionRepo;
    }

    /**
     * Display a listing of the UserConnexion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userConnexionRepository->pushCriteria(new RequestCriteria($request));
        $userConnexions = $this->userConnexionRepository->all();

        return view('user_connexions.index')
            ->with('userConnexions', $userConnexions);
    }

    /**
     * Show the form for creating a new UserConnexion.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_connexions.create');
    }

    /**
     * Store a newly created UserConnexion in storage.
     *
     * @param CreateUserConnexionRequest $request
     *
     * @return Response
     */
    public function store(CreateUserConnexionRequest $request)
    {
        $input = $request->all();

        $userConnexion = $this->userConnexionRepository->create($input);

        Flash::success('User Connexion saved successfully.');

        return redirect(route('userConnexions.index'));
    }

    /**
     * Display the specified UserConnexion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            Flash::error('User Connexion not found');

            return redirect(route('userConnexions.index'));
        }

        return view('user_connexions.show')->with('userConnexion', $userConnexion);
    }

    /**
     * Show the form for editing the specified UserConnexion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            Flash::error('User Connexion not found');

            return redirect(route('userConnexions.index'));
        }

        return view('user_connexions.edit')->with('userConnexion', $userConnexion);
    }

    /**
     * Update the specified UserConnexion in storage.
     *
     * @param  int              $id
     * @param UpdateUserConnexionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserConnexionRequest $request)
    {
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            Flash::error('User Connexion not found');

            return redirect(route('userConnexions.index'));
        }

        $userConnexion = $this->userConnexionRepository->update($request->all(), $id);

        Flash::success('User Connexion updated successfully.');

        return redirect(route('userConnexions.index'));
    }

    /**
     * Remove the specified UserConnexion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userConnexion = $this->userConnexionRepository->findWithoutFail($id);

        if (empty($userConnexion)) {
            Flash::error('User Connexion not found');

            return redirect(route('userConnexions.index'));
        }

        $this->userConnexionRepository->delete($id);

        Flash::success('User Connexion deleted successfully.');

        return redirect(route('userConnexions.index'));
    }
}
