<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransactionCategoryRequest;
use App\Http\Requests\UpdateTransactionCategoryRequest;
use App\Repositories\TransactionCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TransactionCategoryController extends AppBaseController
{
    /** @var  TransactionCategoryRepository */
    private $transactionCategoryRepository;

    public function __construct(TransactionCategoryRepository $transactionCategoryRepo)
    {
        $this->transactionCategoryRepository = $transactionCategoryRepo;
    }

    /**
     * Display a listing of the TransactionCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->transactionCategoryRepository->pushCriteria(new RequestCriteria($request));
        $transactionCategories = $this->transactionCategoryRepository->all();

        return view('transaction_categories.index')
            ->with('transactionCategories', $transactionCategories);
    }

    /**
     * Show the form for creating a new TransactionCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction_categories.create');
    }

    /**
     * Store a newly created TransactionCategory in storage.
     *
     * @param CreateTransactionCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionCategoryRequest $request)
    {
        $input = $request->all();

        $transactionCategory = $this->transactionCategoryRepository->create($input);

        Flash::success('Transaction Category saved successfully.');

        return redirect(route('transactionCategories.index'));
    }

    /**
     * Display the specified TransactionCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            Flash::error('Transaction Category not found');

            return redirect(route('transactionCategories.index'));
        }

        return view('transaction_categories.show')->with('transactionCategory', $transactionCategory);
    }

    /**
     * Show the form for editing the specified TransactionCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            Flash::error('Transaction Category not found');

            return redirect(route('transactionCategories.index'));
        }

        return view('transaction_categories.edit')->with('transactionCategory', $transactionCategory);
    }

    /**
     * Update the specified TransactionCategory in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionCategoryRequest $request)
    {
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            Flash::error('Transaction Category not found');

            return redirect(route('transactionCategories.index'));
        }

        $transactionCategory = $this->transactionCategoryRepository->update($request->all(), $id);

        Flash::success('Transaction Category updated successfully.');

        return redirect(route('transactionCategories.index'));
    }

    /**
     * Remove the specified TransactionCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transactionCategory = $this->transactionCategoryRepository->findWithoutFail($id);

        if (empty($transactionCategory)) {
            Flash::error('Transaction Category not found');

            return redirect(route('transactionCategories.index'));
        }

        $this->transactionCategoryRepository->delete($id);

        Flash::success('Transaction Category deleted successfully.');

        return redirect(route('transactionCategories.index'));
    }
}
