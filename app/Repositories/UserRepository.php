<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:11 pm UTC
 *
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'facebook_id',
        'twitter_id',
        'google_id',
        'name',
        'email',
        'phone_number',
        'photo',
        'password',
        'activated'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
