<?php

namespace App\Repositories;

use App\Models\Wallet;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WalletRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:28 pm UTC
 *
 * @method Wallet findWithoutFail($id, $columns = ['*'])
 * @method Wallet find($id, $columns = ['*'])
 * @method Wallet first($columns = ['*'])
*/
class WalletRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'user_id',
        'name',
        'init_balance',
        'currency'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wallet::class;
    }
}
