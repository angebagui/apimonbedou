<?php

namespace App\Repositories;

use App\Models\TypeTransactionCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeTransactionCategoryRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:35 pm UTC
 *
 * @method TypeTransactionCategory findWithoutFail($id, $columns = ['*'])
 * @method TypeTransactionCategory find($id, $columns = ['*'])
 * @method TypeTransactionCategory first($columns = ['*'])
*/
class TypeTransactionCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeTransactionCategory::class;
    }
}
