<?php

namespace App\Repositories;

use App\Models\UserLocation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserLocationRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:26 pm UTC
 *
 * @method UserLocation findWithoutFail($id, $columns = ['*'])
 * @method UserLocation find($id, $columns = ['*'])
 * @method UserLocation first($columns = ['*'])
*/
class UserLocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'user_id',
        'location_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserLocation::class;
    }
}
