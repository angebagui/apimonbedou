<?php

namespace App\Repositories;

use App\Models\Budget;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BudgetRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:49 pm UTC
 *
 * @method Budget findWithoutFail($id, $columns = ['*'])
 * @method Budget find($id, $columns = ['*'])
 * @method Budget first($columns = ['*'])
*/
class BudgetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'name',
        'amount',
        'currency',
        'transaction_category_id',
        'periode',
        'start_date',
        'notification_enabled',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Budget::class;
    }
}
