<?php

namespace App\Repositories;

use App\Models\UserConnexion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserConnexionRepository
 * @package App\Repositories
 * @version November 24, 2017, 8:51 pm UTC
 *
 * @method UserConnexion findWithoutFail($id, $columns = ['*'])
 * @method UserConnexion find($id, $columns = ['*'])
 * @method UserConnexion first($columns = ['*'])
*/
class UserConnexionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'device_model',
        'device_os',
        'device_id',
        'device_info',
        'network',
        'latitude',
        'longitude',
        'success'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserConnexion::class;
    }
}
