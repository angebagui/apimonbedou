<?php

namespace App\Repositories;

use App\Models\TransactionCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TransactionCategoryRepository
 * @package App\Repositories
 * @version November 24, 2017, 7:38 pm UTC
 *
 * @method TransactionCategory findWithoutFail($id, $columns = ['*'])
 * @method TransactionCategory find($id, $columns = ['*'])
 * @method TransactionCategory first($columns = ['*'])
*/
class TransactionCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'type_category_id',
        'name',
        'language',
        'logo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TransactionCategory::class;
    }
}
