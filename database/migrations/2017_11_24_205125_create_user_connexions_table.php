<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserConnexionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_connexions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_model');
            $table->string('device_os');
            $table->string('device_id');
            $table->string('device_info');
            $table->string('network');
            $table->string('latitude');
            $table->string('longitude');
            $table->boolean('success');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_connexions');
    }
}
