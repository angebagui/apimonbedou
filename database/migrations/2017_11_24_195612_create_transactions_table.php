<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_id')->nullable();
            $table->bigInteger('transaction_category_id')->nullable();
            $table->bigInteger('wallet_id')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->longText('note')->nullable();
            $table->double('location_lat')->nullable();
            $table->double('location_lng')->nullable();
            $table->string('location_address')->nullable();
            $table->string('location_google_ref')->nullable();
            $table->string('picture')->nullable();
            $table->boolean('is_repeated')->nullable();
            $table->integer('repeated_by');
            $table->dateTime('done_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
